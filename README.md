# Vagrant

Allumer les VMs vagrant
```powershell
vagrant up
```
Allumer la VM ansible
```powershell
vagrant up ansible
```
Eteindre les VMs vagrant
```powershell
vagrant halt
```
Eteindre la VM ansible
```powershell
vagrant halt ansible
```
Détruire les VMs vagrant
```powershell
vagrant destroy
```
Détruire la VM ansible
```powershell
vagrant destroy ansible
```
Créé clé ssh
```powershell
ssh-keygen.exe -t ed25519
```
Ajouter clé ssh dans l'agent
```powershell
ssh-add
```
Lister les clé ssh dans l'agent
```powershell
ssh-add -l
```
